import {ModuleWithProviders, NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {ProductFormComponent} from "./product-form/product-form.component";
import {ProductShowComponent} from "./product-show/product-show.component";

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    ProductFormComponent,
    ProductShowComponent,
  ],
  entryComponents: [],
})
export class ProductModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ProductModule,
      providers: [],
    };
  }

}
