import {Component, Input} from '@angular/core';
import {ExamService} from "./client/exam.service";
import {Product} from "./model/product";
import {FormGroup, FormBuilder} from "@angular/forms";

const mainStyle = `
	:host {
		width: 100%;
		height: 100%;
	}
`;

@Component({
  selector: 'app-root',
  template: `
		<router-outlet></router-outlet>
	`,
  styles: [mainStyle],
})
export class AppComponent {

}
