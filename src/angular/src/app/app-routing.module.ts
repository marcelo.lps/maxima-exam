import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AppComponent} from "./app.component";
import {ProductShowComponent} from "./product/product-show/product-show.component";
import {ProductFormComponent} from "./product/product-form/product-form.component";


const appRoutes: Routes = [
  {
    path: '',
    component: ProductFormComponent,
    pathMatch: 'full',
  },
  {
    path: 'product/create',
    component: ProductFormComponent,
  },
  {
    path: 'product/:id',
    component: ProductShowComponent,
  }
];

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    canActivate: [],
    children: [
      ...appRoutes,
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
