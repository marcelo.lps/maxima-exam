package br.com.maxima.service;

import br.com.maxima.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductService extends AbstractCrudService<Product> {

    @Autowired
    protected ProductService(JpaRepository<Product, Long> dao) {
        super(dao);
    }
}
